#!/bin/bash
set -euf -o pipefail
shopt -s inherit_errexit

hash curl
hash dirname
hash mktemp
hash python3
hash xorrisofs

if ! python3 -c "import koji" &>/dev/null
then
    echo "Missing python module koji. You can install it via `pip install koji`" 1>&2
    exit 1
fi

function error {
    echo "Error on line $(caller)!" 1>&2
    exit 1
}
trap error ERR

function cleanup {
    if [[ -d ${TMPDIR:-} ]]
    then
        rm -rf $TMPDIR
    fi
}
trap cleanup EXIT

SCRIPTS="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

function usage {
    cat << EOL
NAME
    $0 - Kmods SIG Driver Disc Creator

SYNOPSIS
    $0 --release <release> --arch <arch> [--kernel <version>] [--rebuild] --output <iso> package [package ...]

DESCRIPTION
    $0 creates Driver Disc containing specified kmod packages provided by the
    Kmods SIG. Regular expressions (using Python Regular Expression Syntax) are
    supported to match multiple packages.

OPTIONS
    --arch
        Create Driver Disc for architecture <arch>.
        Possible values: aarch64, ppc64le, or x86_64.

    -h, --help
        Print this message.

    --dry-run
        Perform a trial run. No Driver Disc is created and no packages are
        downloaded. URLs to packages to be downloaded are written to standard
        output. Disables --list.

    --kernel <version>
        Create Driver Disc containing kmod packages compatible to the specified
        kernel version.

    --list
        List packages included in Driver Disc iso.

    --output <iso>
        Specify the output file for the Driver Disc iso.

    --rebuild
        Enable kmod packages replacing kernel modules included in Enterprise Linux.

    --release <release>
        Create Driver Disc for EL major release <release>.
        Possible values: 8, 8s, or 9s.
EOL
    exit 1
}

TAGS=(kmods\$RELEASE-packages-main-release)

LIST='false'
DRYRUN='false'

options=$(getopt -o h --long arch: --long dry-run --long help --long kernel: --long list --long output: --long rebuild --long release: -- "$@") ||
    error "Incorrect/Unsupported options provided."
eval set -- "$options"
while true; do
    case "$1" in
        --arch)
            ARCH="$2"
            shift
            ;;
        --dry-run)
            DRYRUN='true'
            ;;
        -h|--help)
            usage
            ;;
        --kernel)
            KERNEL="$2"
            shift
            ;;
        --list)
            LIST='true'
            ;;
        --output)
            OUTPUT="$2"
            shift
            ;;
        --rebuild)
            TAGS+=(kmods\$RELEASE-packages-rebuild-release)
            ;;
        --release)
            RELEASE="$2"
            shift
            ;;
        --)
            shift
            break
            ;;
    esac
    shift
done

case ${RELEASE:-} in
    8|8s|9s)
        ;;
    *)
        echo "Unsupported or no release specified!" 1>&2
        usage
        ;;
esac

eval TAGS=(${TAGS[@]})

case ${ARCH:-} in
    aarch64|ppc64le|x86_64)
        ;;
    *)
        echo "Unsupported or no architecture specified!" 1>&2
        usage
        ;;
esac

if [[ -z ${OUTPUT:-} ]]
then
    echo "No output file specified!" 1>&2
    usage
fi

OUTPUT=${OUTPUT%.iso}
OUTPUT=${OUTPUT/%/.iso}

if [[ -e $OUTPUT ]]
then
    echo "Output ($OUTPUT) already exists!" 1>&2
    exit 1
fi

if ! [[ -w $(dirname $OUTPUT) ]]
then
    echo "Output directory ($(dirname $OUTPUT)) is not writable!" 1>&2
    exit 1
fi

URLS=$(cat << EOT | python3 - --arch $ARCH ${TAGS[@]/#/--tag } $([[ -z ${KERNEL:-} ]] || echo "--kernel $KERNEL") "$@"
import argparse
import koji
import packaging.version
import random
import re
import urllib.request
from xml.etree import ElementTree

parser = argparse.ArgumentParser(
    description='Find packages in CBS. Prints URLs to found packages.')

parser.add_argument('--arch', metavar='arch', required=True,
                    help='List packages of given architectures.')
parser.add_argument('--tag', metavar='tag', required=True,
                    action='append', help='List packages in tag. Can be used multiple times.')

parser.add_argument('--kernel', metavar='version',
                    help='List packages compatible with kernel version.')
parser.add_argument('--url', metavar='url',
                    default="https://cbs.centos.org", help='URL to koji server. Default: https://cbs,centos.org')

parser.add_argument('--mirror', default=False, action='store_true',
                    help='Download from main mirror instead of mirrorlist.')

parser.add_argument('packages', metavar='package', nargs="+",
                    help='List of packages to search in koji. Supports regular expressions.')

args = parser.parse_args()

session = koji.ClientSession(args.url + "/kojihub")

for tag in args.tag:
    sigrel, project, version, taglevel = tag.split('-')
    m = re.search('(.*)([0-9]+.*)', sigrel)
    if not m:
        continue
    sig = m.group(1)
    release = m.group(2)
    mirror_release = re.sub('s', '-stream', release)

    mirror = None
    if taglevel == "release":
        if not args.mirror:
            if release.startswith('8'):
                mirror = "http://mirrorlist.centos.org/?release={}&arch={}&repo={}-{}-{}".format(
                    mirror_release, args.arch, sig, project, version)
                try:
                    response = urllib.request.urlopen(mirror)
                    data = response.read()
                    mirror = random.choice(
                        [s for s in data.decode('utf-8').splitlines() if s])
                except:
                    mirror = None
            elif release.startswith('9'):
                mirror = "https://mirrors.centos.org/metalink?repo=centos-{}-sig-{}-{}-{}&arch={}&protocol=https,http".format(
                    sig, project, version, mirror_release, args.arch)
                try:
                    response = urllib.request.urlopen(mirror)
                    data = response.read()
                    mirror = random.choice(
                        [child.text.removesuffix('/repodata/repomd.xml') for child in ElementTree.fromstring(data).iter('{http://www.metalinker.org/}url')])
                except Exception as e:
                    print(e)
                    mirror = None

        if not mirror:
            if release.startswith('8'):
                mirror = "http://mirror.centos.org/centos/{}/{}/{}/{}-{}".format(
                    mirror_release, sig, args.arch, project, version)
            elif release.startswith('9'):
                mirror = "http://mirror.stream.centos.org/SIGs/{}/{}/{}/{}-{}".format(
                    mirror_release, sig, args.arch, project, version)

    elif taglevel == "testing":
        mirror = "http://buildlogs.centos.org/centos/{}/{}/{}/{}-{}".format(
            mirror_release, sig, args.arch, project, version)

    mirror = mirror.rstrip('/')

    if session.getTag(tag):
        for rpm in session.listTaggedRPMS(tag, latest=0 if args.kernel else 1, arch=args.arch)[0]:
            if any(regex.fullmatch(rpm['name']) for regex in map(re.compile, args.packages)):
                try:
                    provides = [p['version'] for p in session.getRPMDeps(
                        rpm['id'], depType=1) if p['name'] == 'kernel-modules'][0][:-len(args.arch)-1]
                except:
                    continue
                if args.kernel:
                    if packaging.version.parse(provides) <= packaging.version.parse(args.kernel):
                        args.packages.remove(rpm['name'])
                    else:
                        continue
                if mirror:
                    url = "{}/Packages/{}/{}-{}-{}.{}.rpm".format(mirror,
                          rpm['name'][0], rpm['name'], rpm['version'], rpm['release'], rpm['arch'])
                else:
                    url = "{}/kojifiles/packages/{}/{}/{}/{}/{}-{}-{}.{}.rpm".format(args.url,
                          rpm['name'], rpm['version'], rpm['release'], args.arch, rpm['name'], rpm['version'], rpm['release'], rpm['arch'])
                print(url)
EOT
)

if [[ ! -z $URLS ]]
then
    TMPDIR=$(mktemp -d)

    mkdir -p $TMPDIR/dd/rpms/$ARCH
    echo -e "Driver Update Disk version 3\c" > $TMPDIR/dd/rhdd3

    cd $TMPDIR/dd/rpms/$ARCH

    while read URL
    do
        if $DRYRUN
        then
            echo $URL
        else
            if $LIST
            then
                echo $(basename $URL)
            fi
            curl -f -s -O $URL
        fi
    done <<< $URLS

    cd $SCRIPTS
    if ! $DRYRUN
    then
        xorrisofs -quiet -lR -V OEMDRV -input-charset utf8 -o $OUTPUT $TMPDIR/dd
    fi
else
    echo "No packages found!" 1>&2
    exit 1
fi
